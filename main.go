package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"regexp"
	"strings"

	yaml "gopkg.in/yaml.v2"

	"gitlab.com/gl-infra/gitlab-patcher/knife"
)

func getCR() string {
	cr := os.Getenv("GITLAB_CHEF_REPO_DIR")
	if cr == "" {
		log.Fatal("Can't get env var GITLAB_CHEF_REPO_DIR")
	}
	return cr
}

type conf struct {
	Mode           string   `yaml:"mode"`
	Src            []string `yaml:"src_roles"`
	Dst            []string `yaml:"dst_roles"`
	ExtraModifiers string   `yaml:"extra_modifiers"`
}

//rolesQuery is not exported from knife, had to reimplement
func rQ(a []string) string {
	for i, r := range a {
		a[i] = "roles:" + r
	}
	return strings.Join(a, " OR ")
}

// same for name query
func nQ(a []string) string {
	for i, r := range a {
		a[i] = "name:" + r
	}
	return strings.Join(a, " OR ")
}

func (cPtr *conf) buildConfig(f string, kPtr *knife.Knife) *conf {
	raw, err := ioutil.ReadFile(f)
	if err != nil {
		log.Fatalf("Can't read file %s: '%v'", f, err)
	}
	if err = yaml.Unmarshal(raw, &cPtr); err != nil {
		log.Fatalf("Unmarshall Error: '%v'", err)
	}
	// validate it immediately, no point in having broken config
	if cPtr.Mode != "block" && cPtr.Mode != "clean" {
		log.Fatalf("mode should be 'block' or 'clean'")
	}
	if strings.Contains(cPtr.ExtraModifiers, "-j") { // also matches --jump
		log.Fatal("Specifying jump is not supported")
	}
	validIptablesChars := regexp.MustCompile(`^[A-Za-z0-9!_. -]+$`).MatchString
	if !validIptablesChars(cPtr.ExtraModifiers) {
		log.Fatal("Yes, you can inject shit and shoot yourself in the foot. Don't do it.")
	}
	if len(cPtr.Src) < 1 || len(cPtr.Dst) < 1 {
		log.Fatalf("Both src and dst roles should be specified")
	} else {
		// resolve everything in place too
		cPtr.Src, err = kPtr.ResolveHostnames(rQ(cPtr.Src))
		if err != nil {
			log.Fatalf("%v\n", err)
		}
		cPtr.Dst, err = kPtr.ResolveHostnames(rQ(cPtr.Dst))
		if err != nil {
			log.Fatalf("%v\n", err)
		}
	}
	return cPtr
}

func main() {
	flag.Usage = func() {
		flag.PrintDefaults()
	}
	ymlPtr := flag.String("yml", "block.yml", "Path to instructions file")
	crPtr := flag.String("chef-repo", getCR(), "Chef repo path, defaults to GITLAB_CHEF_REPO_DIR env var")
	flag.Parse()
	c := conf{}
	k := knife.New(*crPtr)
	c.buildConfig(*ymlPtr, &k)

	cmd := "hostname"
	if c.Mode == "block" {
		fmt.Printf("Will DROP packets to %s in the OUTPUT chain of %s\n",
			c.Dst, c.Src)
		command := `
		sudo /sbin/iptables -A OUTPUT -d %[1]s %[2]s -j LOG --log-prefix 'network-gnome: ' --match limit --limit '2/second' --log-level 7 && sudo /sbin/iptables -A OUTPUT -d %[1]s %[2]s -j DROP && hostname
		`
		cmd = fmt.Sprintf(command, strings.Join(c.Dst, ","), c.ExtraModifiers)
	} else if c.Mode == "clean" {
		fmt.Printf("Will FLUSH OUTPUT chain of %s\n", c.Src)
		cmd = "sudo /sbin/iptables -F OUTPUT && hostname"
	} else {
		log.Fatal("Shouldn't be here at all!\n")
	}
	if err := k.Run(os.Stdout, "ssh", nQ(c.Src), cmd); err != nil {
		log.Fatalf("Error running knife command: '%v'\n", err)
	}
}
