### Description
network-gnome is a small utility to inject iptables rules across our servers in a (semi-) reproducible way.

Created as a tool to ease tests of https://gitlab.com/gitlab-com/infrastructure/issues/2446. Currently, it
heavily relies on the fact that our egress traffic is not limited in any way, therefore in order to simulate
packet loss from SRC to DST its easy to just insert `-A OUTPUT -d DST -j DROP` at the _SRC_, and flush the
iptables to reset the state to before-the-test condition.

Currently it supports injecting many-to-many blocks to simulate normal cloud provider behavior.

### Usage:
 - compile
 - prepare configuration file (see block.yml for details)
 - run `./main -yml your.yml`

Requires working knife setup and uses knife module from gitlab-patcher.

### TODO:
 - tests
 - pipelines
 - destination querying from chef instead of resolving on endpoint system
 - better logging
 - failsafe switch (like, `iptables -F OUTPUT | at now +5 min`
